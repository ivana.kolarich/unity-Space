﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnTriggerEnter(Collider col)
    {
        Debug.Log("Collided with picup");
        if (col.transform.CompareTag("Player")){
            PickupHit();
        }
    }

    public void PickupHit()
    {
        EventManager.ScorePoints(1);
        Destroy(gameObject);
    }
}
