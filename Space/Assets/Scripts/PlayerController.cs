﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class PlayerController : MonoBehaviour {
    [SerializeField]
    Laser[] laser;

    private PlayerMovement playerMovement;  //Reference to PlayerMovement script
    private Rigidbody rigidBody;
    private bool gasToggle = false;

    // Use this for initialization
    void Start() {
        rigidBody = GetComponent<Rigidbody>();
        playerMovement = GetComponent<PlayerMovement>();
        gasToggle = false;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            EventManager.PauseGame();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            foreach (Laser l in laser)
            {
                Vector3 pos = transform.position + (transform.forward * l.Distance);
                l.FireLaser();
            }
        }
    }

    void FixedUpdate () {


        //Debug.DrawRay(transform.position, transform.forward, Color.yellow);
        //Debug.DrawRay(transform.position, transform.up, Color.red);
        
        // Toggle moveForward.
        if (Input.GetButtonDown("Gas"))
        {
            GasToggle();
        }
        float gas = (gasToggle) ? Mathf.Lerp(0f, 1f, Time.deltaTime * 3) : Mathf.Lerp(1f, 0f, Time.deltaTime * 3);

        if (Input.GetButton("Fire3"))
        {
            gas = gas * 5;
        }

       
        playerMovement.MoveForward(gas);
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float roll = -Input.GetAxis("Roll");

        playerMovement.Turn(horizontal, vertical, roll);

        if (Input.GetButtonDown("Jump"))
        {
            playerMovement.MoveLeftOrRight(horizontal);
        }

    }


    void GasToggle()
    {
        gasToggle = !gasToggle;
    }
}
