﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField] int maxHealth = 5;
    [SerializeField] int curHealth;
    [SerializeField] float regenerationRate = 2f;
    [SerializeField] int regenerateAmount = 1;

    // Use this for initialization
    void Start()
    {
        curHealth = maxHealth;
        InvokeRepeating("Regenerate", regenerationRate, regenerationRate);
    }

    void Regenerate()
    {
        if (curHealth < maxHealth)
        {
            curHealth += regenerateAmount;
        }
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
            // CancelInvoke();
        }
    }

    public void TageDamage(int dmg = 1)
    {
        curHealth -= dmg;
        if (curHealth < 1)
        {
            Debug.Log("Score points");
            EventManager.ScorePoints(maxHealth);
            Destroy(gameObject, 0.3f);
            GetComponent<Explosion>().BlowUp();
        }
    }



}
