﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow3D : MonoBehaviour {

    public Transform target;
    private Transform myT;
    public Vector3 distance = new Vector3(0f, 1f, -5f);
    public Vector3 velocity = Vector3.one;
    public float positionDamping = 2f;
    public float rotationDamping = 0.5f;
    public float distanceDamp = 0.5f;


    // Use this for initialization
    void Start () {

        myT = transform;
	}
	
	// LateUpdate is called once per frame but after Update and FixedUpdate.
    

	void LateUpdate () {
        if (!FindTarget())
            return;
        SmoothFollow();
        //if (target == null)
        //    return;
        //// Where should camera be.
        //Vector3 wantedPosition = target.position + (target.rotation * distance);
        //// Linear enterpolation, smoothing camera motion.
        //Vector3 currentPosition = Vector3.Lerp(transform.position, wantedPosition, positionDamping * Time.deltaTime);

        //transform.position = currentPosition;

        //// End rotation of the camera. Rotation towards player.
        //Quaternion wantedRotation = Quaternion.LookRotation(target.position - transform.position, target.up);

        //transform.rotation = wantedRotation;
    }

    void SmoothFollow()
    {
        Vector3 toPos = target.position + (target.rotation * distance);
        Vector3 curPos = Vector3.SmoothDamp(myT.position, toPos, ref velocity, distanceDamp);
        myT.position = curPos;
        myT.LookAt(target, target.up);
    }

    bool FindTarget()
    {
        if (target == null)
        {
            GameObject temp = GameObject.FindGameObjectWithTag("PlayerCamera");
            if (temp != null)
            {
                target = temp.transform;
            }
        }

        if (target == null)
        {
            return false;
        }
        return true;
    }
}
