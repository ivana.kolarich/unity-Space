﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] GameObject enemyLegacy;
    [SerializeField] float spawnTimer = 5f;
    [SerializeField] int maxEnemies = 2;
    private int currentNumOfEnemies = 0;

    private void Start()
    {
        //StartSpawning();
    }

    private void OnEnable()
    {
        EventManager.onStartGame += StartSpawning;
        EventManager.onPlayerDeath += StopSpawning;
        EventManager.onEnemyDeath += EnemyDeath;
    }

    private void OnDisable()
    {
        EventManager.onStartGame -= StartSpawning;
        EventManager.onPlayerDeath -= StopSpawning;
        EventManager.onEnemyDeath -= EnemyDeath;

    }

    void EnemyDeath(Vector3 position)
    {
        SpawnEnemyLegacy(position);
        currentNumOfEnemies -= 1;
    }
    
    void SpawnEnemyLegacy(Vector3 positionOfTheDeadEnemy)
    {          
        Instantiate(enemyLegacy, positionOfTheDeadEnemy, Quaternion.identity);
    }

    void SpawnEnemy()
    {
        if (currentNumOfEnemies < maxEnemies)
        {
            Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            currentNumOfEnemies += 1;
        }
    }

    void StartSpawning()
    {
        Debug.Log("StartSpawning");
        currentNumOfEnemies = 0;
        InvokeRepeating("SpawnEnemy", spawnTimer, spawnTimer);
    }

    void StopSpawning()
    {
        CancelInvoke();
    }
}
