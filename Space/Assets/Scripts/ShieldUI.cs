﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gettinf events for updating Shield bar on the bottom of the screen.
/// </summary>
public class ShieldUI : MonoBehaviour {
    [SerializeField] RectTransform barRectTransform;
    float maxWidth;

    private void Awake()
    {
        maxWidth = barRectTransform.rect.width;
    }

    private void OnEnable()
    {
        EventManager.onTakeDamage += UpdateShieldDisplay;
    }

    private void onDisable()
    {
        EventManager.onTakeDamage -= UpdateShieldDisplay;
    }

    void UpdateShieldDisplay(float percentage)
    {
        barRectTransform.sizeDelta = new Vector2(maxWidth * percentage, 10f);
    }

}
