﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float movementSpeed =  50f;
    public float turnSpeed = 1f;
    public float maxAngularVelocity = 1f;

    private Rigidbody rb;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
    }

    public void MoveForward(float _move)
    {
        // Allow forward movement.
        if (_move > 0)
        {
            rb.velocity = transform.forward * movementSpeed * _move;
            //Debug.DrawRay(transform.position, rb.velocity, Color.blue);
        }
    }

    public void MoveLeftOrRight(float _move)
    {
        // Allow forward movement.
        if (_move != 0)
        {
            rb.velocity = transform.right * movementSpeed * 10 * _move;
            //Debug.DrawRay(transform.position, rb.velocity, Color.blue);
        }
    }



    /// <summary>
    /// Apllies forces to Rigidbody in order to rotate the body. When parameters equals 0,
    /// maxAngularVelocity of rigidBody. Is set to 0 and the object stops rotating.
    /// </summary>
    /// <param name="_rotYaw"> "Input Horizontal axis. (Left Right)"</param>
    /// <param name="_rotPitch">"Input Vertial axis (Up Down)"</param>
    /// <param name="_rotRoll">"Input Roll axis." (Rotation)</param>
    public void Turn(float _rotYaw, float _rotPitch, float _rotRoll)
    {
        float yaw =   _rotYaw * turnSpeed; 
        float pitch = _rotPitch * turnSpeed;
        float roll =  _rotRoll * turnSpeed;

        if (pitch != 0 || yaw != 0 || roll != 0)
        {
            rb.maxAngularVelocity = turnSpeed;
            rb.AddRelativeTorque(Vector3.left * pitch * (-1), ForceMode.Impulse);
            rb.AddRelativeTorque(Vector3.up * yaw, ForceMode.Impulse);
            rb.AddRelativeTorque(Vector3.forward * roll, ForceMode.Impulse);
            Debug.DrawRay(transform.position, rb.angularVelocity, Color.magenta);
        } else
        {
            float angleSpeed = Mathf.Lerp(rb.angularDrag, 0, Time.deltaTime);
            rb.maxAngularVelocity = angleSpeed;
        }
    }
}
