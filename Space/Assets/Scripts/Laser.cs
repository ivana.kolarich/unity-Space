﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour {

    public float laserOffTime = 0.2f;
    public float fireDelay = 2f;
    public float reflexDelay = 0f;
    public float maxDistance = 300f;

    LineRenderer lr;
    Light laserLight;
    bool canFire = true;
	// Use this for initialization
	void Awake()
    {
        lr = GetComponent<LineRenderer>();
        laserLight = GetComponent<Light>();
    }

    void Start()
    {
        lr.enabled = false;
        laserLight.enabled = false;
        canFire = true;
    }

    //private void Update()
    //{
    //    Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * maxDistance, Color.cyan);
    //}

    /// <summary>
    /// CastRay to se if the laser will hit something.
    /// </summary>
    /// <returns>Position if we hit someting, or position of the maximum distance of the laser.</returns>
    Vector3 CastRay()
    {
        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward) * maxDistance;

        if(Physics.Raycast(transform.position, fwd, out hit))
        {
            // We have hit something
            SpawnExplosion(hit.point, hit.transform);
            return hit.point;
        }
        else
        {
            // Missed the target
            return transform.position + (transform.forward * maxDistance);
        }
        
    }

    /// <summary>
    /// Gets the explosion component on the target and spawns (animates) explosion.
    /// </summary>
    /// <param name="hitPosition">The position where the laser hit the object.</param>
    /// <param name="target">Transform of the object that has been hit.</param>
    void SpawnExplosion(Vector3 hitPosition, Transform target)
    {
        // Get explosion of that object and add explosion force to it.
        Explosion temp = target.GetComponent<Explosion>();
        if (temp != null) {}
            temp.AddForce(hitPosition, transform);
                //temp.IveBeenHit(hitPosition);

    }

    /// <summary>
    /// Fires laser. Before firing CastRay() is called to tetermine the end position of the laser.
    /// </summary>
    public void FireLaser () {
        Vector3 pos = CastRay();
        FireLaser(pos);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="targetPosition">The position of the target of the laser. Destination of the laser.</param>
    /// <param name="target">Transform of the target. If we know that the target has been hit. If this is null we just fire laser.</param>
    public void FireLaser(Vector3 targetPosition, Transform target = null)
    {
        if (canFire)
        {
            if (target != null)
            {
                SpawnExplosion(targetPosition, target);
            }
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, targetPosition);
            // Enable lasers and laser lights
            lr.enabled = true;
            laserLight.enabled = true;

            // Wait for some time before firing again.
            canFire = false;
            Invoke("TurnOffLaser", laserOffTime);
            Invoke("CanFire", fireDelay);
        }
    }
 

    void CanFire()
    {
        canFire = true;
    }

    void TurnOffLaser()
    {
        lr.enabled = false;
        laserLight.enabled = false;
    }

    public float Distance
    {
        get { return maxDistance; }
    }
}
