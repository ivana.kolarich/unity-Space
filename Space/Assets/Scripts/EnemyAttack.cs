﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {
    [SerializeField] Transform target;
    [SerializeField] Laser[] lasers;
    Vector3 hitPosition;

    // Update is called once per frame
    void Update()
    {
        if (!FindTarget())
        {
            return;
        }
        if (InFront() && HaveLineOfSightRaycast())
        {
            FireLaser();
        };
    }

    /// <summary>
    /// Check if an object is infront of curent object.
    /// </summary>
    /// <returns>True if the target is in front of current object.</returns>
    bool InFront()
    {
        Vector3 directionToTarget = transform.position - target.position;
        float angle = Vector3.Angle(transform.forward, directionToTarget);
        
        if (Mathf.Abs(angle) > 170 && Mathf.Abs(angle) < 190)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Check if we have clear sight of the object. There is no other object between current object (enemy) and the target object.
    /// </summary>
    /// <returns></returns>
	bool HaveLineOfSightRaycast()
    {
        RaycastHit hit;
        Vector3 direction = target.position - transform.position;
        Debug.DrawRay(transform.position, direction, Color.yellow);
        foreach (Laser laser in lasers)
        {
            if (Physics.Raycast(laser.transform.position, direction, out hit, laser.Distance))
            {
                if (hit.transform.CompareTag("Player"))
                {
                    hitPosition = hit.transform.position;
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Fire all lasers.
    /// </summary>
    void FireLaser()
    {
        foreach (Laser laser in lasers)
        {
            laser.FireLaser(hitPosition, target);
        }
    }

    /// <summary>
    /// Find the target with tag "Player" and set it as target.
    /// </summary>
    /// <returns>Returns true if the target is found</returns>
    bool FindTarget()
    {
        if (target == null)
        {
            GameObject temp = GameObject.FindGameObjectWithTag("Player");
            if (temp != null)
            {
                target = temp.transform;
            }
        }

        if (target == null)
        {
            return false;
        }
        return true;
    }
}
