﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

    public delegate void StartGameDelegate();
    public static StartGameDelegate onStartGame;
    public static StartGameDelegate onPauseGame;
    public static StartGameDelegate onPlayerDeath;

    public delegate void TakeDamageDelegate(float amt);
    public static TakeDamageDelegate onTakeDamage;

    public delegate void ScorePointsDelegate(int scorePoints);
    public static ScorePointsDelegate onScorePoints;

    public delegate void EnemyDeathDelegate(Vector3 position);
    public static EnemyDeathDelegate onEnemyDeath;

    public static void StartGame()
    {
        if (onStartGame != null)
        {
            onStartGame();
        }
    }

    public static void PauseGame()
    {
        if (onPauseGame != null)
        {
            onPauseGame();
        }
    }

    public static void PlayerDeath()
    {
        if (onPlayerDeath != null)
        {
            onPlayerDeath();
        }
    }

    public static void TakeDamage(float percent)
    {
        if (onTakeDamage != null)
        {
            onTakeDamage(percent);
        }
    }

    public static void EnemyDeath(Vector3 position)
    {
        if (onEnemyDeath != null)
        {
            onEnemyDeath(position);
        }
    }

    public static void ScorePoints(int scorePoints)
    {
        if (onScorePoints != null)
        {
            onScorePoints(scorePoints);
        }
    }
}
