﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    [SerializeField] Transform target;
    [SerializeField] Transform defend;
    [SerializeField] float movementSpeed = 5f;
    [SerializeField] float rotationalDamp = 50f;
    [SerializeField] float detectionDistance = 20f;
    [SerializeField] float rayCastOffsetV = 1f; // Vertical offset
    [SerializeField] float rayCastOffsetH = 2f; // Horisontal offset
    [SerializeField] float defendDistance = 100f;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!FindTarget())
        {
            return;
        }
        PathFinder();
        Move();
	}

    void Turn()
    {
        Vector3 pos = target.position - transform.position;

        if (defend != null)
        {
            float distDefend = Vector3.Distance(defend.position, transform.position);
            float distTarget = Vector3.Distance(target.position, transform.position);

            if (defendDistance < distTarget) {
                pos = defend.position - transform.position;
            }
        }

        Quaternion rotation = Quaternion.LookRotation(pos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationalDamp * Time.deltaTime);
    }

    void Move()
    {
        transform.position += transform.forward * Time.deltaTime * movementSpeed; 
    }

    void PathFinder()
    {
        RaycastHit hit;
        Vector3 raycastOffset = Vector3.zero;

        Vector3 left = transform.position - transform.right * rayCastOffsetH;
        Vector3 right = transform.position + transform.right * rayCastOffsetH;
        Vector3 up = transform.position + transform.up * rayCastOffsetV;
        Vector3 down = transform.position - transform.up * rayCastOffsetV;

        Debug.DrawRay(left, transform.forward * detectionDistance, Color.cyan);
        Debug.DrawRay(right, transform.forward * detectionDistance, Color.cyan);
        Debug.DrawRay(up, transform.forward * detectionDistance, Color.red);
        Debug.DrawRay(down, transform.forward * detectionDistance, Color.red);

        if(Physics.Raycast(left, transform.forward, out hit, detectionDistance))
        {
            raycastOffset += Vector3.right;
        } else if(Physics.Raycast(right, transform.forward, out hit, detectionDistance))
        {
            raycastOffset -= Vector3.right;
        };

        if (Physics.Raycast(up, transform.forward, out hit, detectionDistance))
        {
            raycastOffset -= Vector3.up;
        }
        else if (Physics.Raycast(down, transform.forward, out hit, detectionDistance))
        {
            raycastOffset += Vector3.up;
        };

        if(raycastOffset != Vector3.zero)
        {
            transform.Rotate(raycastOffset * 5f * Time.deltaTime);
        }
        else
        {
            Turn();
        }
    }

    bool FindTarget()
    {
        if (target == null)
        {
            GameObject temp = GameObject.FindGameObjectWithTag("Player");
            if (temp != null)
            {
                target = temp.transform;
            }
        }

        if (target == null)
        {
            return false;
        }
        return true;
    }

    void FindMainCamera()
    {
        target = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    private void OnEnable()
    {
        EventManager.onPlayerDeath += FindMainCamera;
        EventManager.onStartGame += SelfDestruct;
    }
    private void OnDisable()
    {
        EventManager.onPlayerDeath -= FindMainCamera;
        EventManager.onStartGame -= SelfDestruct;

    }

    void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
