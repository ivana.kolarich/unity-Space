﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShield : MonoBehaviour {
    [SerializeField] int maxHealth = 10;
    [SerializeField] int curHealth;
    [SerializeField] float regenerationRate = 2f;
    [SerializeField] int regenerateAmount = 1;


    private void OnEnable()
    {
        EventManager.onScorePoints += RegenerateBasedOnScore;
    }

    private void OnDisable()
    {
        EventManager.onScorePoints -= RegenerateBasedOnScore;
    }

    // Use this for initialization
    void Start () {
        curHealth = maxHealth;
        InvokeRepeating("Regenerate", regenerationRate, regenerationRate);
    }

    void RegenerateBasedOnScore(int score)
    {
        Debug.Log("Regenerate");
        if (curHealth < maxHealth)
        {
            curHealth = maxHealth;
        }
        if (curHealth > maxHealth)
        {
            curHealth = maxHealth;
            // CancelInvoke();
        }
        EventManager.TakeDamage(curHealth / (float)maxHealth);
    }
	
    void Regenerate()
    {
        if (curHealth < maxHealth)
        {
            curHealth += regenerateAmount;
        }
        if(curHealth > maxHealth)
        {
            curHealth = maxHealth;
           // CancelInvoke();
        }
        EventManager.TakeDamage(curHealth / (float)maxHealth);
    }

    public void TageDamage(int dmg = 1)
    {
        curHealth -= dmg;
        if (curHealth < 0)
        {
            curHealth = 0;
        }
        EventManager.TakeDamage(curHealth/(float)maxHealth);
        if(curHealth < 1)
        {
            GetComponent<Explosion>().BlowUp();
            //remove life
        }
    }
	// Update is called once per frame
	void Update () {
	
	}
}
