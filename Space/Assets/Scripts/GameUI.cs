﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour {
    bool isDisplayed = true;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject gameUI;
    [SerializeField] GameObject playButton;

    [SerializeField] GameObject playerPrefab;
    [SerializeField] GameObject playerStartPosition;

    private void OnEnable()
    {
        EventManager.onStartGame += StartGameUI;
        EventManager.onPlayerDeath += PlayerDeath;
        EventManager.onPauseGame += ShowMainMenu;
    }

    private void OnDisable()
    {
        EventManager.onStartGame -= StartGameUI;
        EventManager.onPlayerDeath -= PlayerDeath;
        EventManager.onPauseGame -= ShowMainMenu;
    }

    void PlayerDeath()
    {
        StartCoroutine("DelayMainMenuDisplay", 4f);
    }

    void ShowMainMenu()
    {
        if (Time.timeScale == 0)
        {
            HideMainMenuOnPause();
        } else {
            ShowMainMenuOnPause();
        }
    }

    void ShowMainMenuOnPause()
    {

        mainMenu.SetActive(true);
        playButton.SetActive(false);
        gameUI.SetActive(false);
        Time.timeScale = 0;
    }

    void HideMainMenuOnPause()
    {
        playButton.SetActive(true);
        mainMenu.SetActive(false);
        gameUI.SetActive(true);
        Time.timeScale = 1;
    }

    IEnumerator DelayMainMenuDisplay(float _time)
    {
        yield return new WaitForSeconds(_time);
        ShowMainMenuOnPause();
        playButton.SetActive(true);
    }

    void StartGameUI()
    {
        if (Time.timeScale == 0)
            Time.timeScale = 1;
        mainMenu.SetActive(false);
        gameUI.SetActive(true);

        Instantiate(playerPrefab, playerStartPosition.transform.position, playerStartPosition.transform.rotation);

    }

}
