﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    [SerializeField]Text hiScoreText;
    [SerializeField]Text scoreText;
    [SerializeField]int score;
    [SerializeField]int hiScore;

    private void Start()
    {
        LoadHiScore();
    }

    private void OnEnable()
    {
        EventManager.onStartGame += ResetScore;
        EventManager.onPlayerDeath += CheckNewHiScore;
        EventManager.onScorePoints += AddScore;
    }

    private void OnDisable()
    {
        EventManager.onStartGame -= ResetScore;
        EventManager.onPlayerDeath -= CheckNewHiScore;
        EventManager.onScorePoints -= AddScore;
    }

    void DisplayScore()
    {
        scoreText.text = score.ToString();
    }

    void ResetScore()
    {
        score = 0;
        DisplayScore();
    }

    void AddScore(int amount)
    {
        Debug.Log("Scorepoints: " + amount);
        score += amount;
        DisplayScore();
    }

    void LoadHiScore()
    {
        hiScore = PlayerPrefs.GetInt("hiScore", 0);
    }

    void CheckNewHiScore()
    {
        if(score > hiScore)
        {
            PlayerPrefs.SetInt("hiScore", score);
            DisplayHighScore();
        }
    }
    
    void DisplayHighScore()
    {
        hiScoreText.text = hiScore.ToString();
    }
}
