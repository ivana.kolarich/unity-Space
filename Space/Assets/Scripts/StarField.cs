﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarField : MonoBehaviour {

    private ParticleSystem.Particle[] points;
    private float starDistanceSqr;
    private float starClipDistanceSqr;

    public Color starColor;
    public int starMax = 600;
    public float starSize = 0.35f;
    public float starDistance = 60f;
    public float starClipDistance = 15f;

    // Use this for initialization
	void Start () {
        starDistanceSqr = starDistance * starDistance;
        starClipDistanceSqr = starClipDistance * starClipDistance;
	}
	
    void CreateStars()
    {
        points = new ParticleSystem.Particle[starMax];

        for (int i = 0; i < starMax; i++)
        {
            points[i].position = Random.insideUnitSphere * starDistance + transform.position;
            points[i].startColor = new Color(starColor.r, starColor.g, starColor.b, starColor.a);
            points[i].startSize = starSize;
        }
    }

    // Update is called once per frame
    void Update () {
        if (points == null)
        {
            CreateStars();
        }

        for (int i = 0; i < starMax; i++)
        {
            if((points[i].position - transform.position).sqrMagnitude > starDistanceSqr)
            {
                points[i].position = Random.insideUnitSphere.normalized * starDistance + transform.position;
            }

            if((points[i].position - transform.position).sqrMagnitude <= starClipDistanceSqr)
            {
                float percentage = (points[i].position - transform.position).sqrMagnitude / starClipDistanceSqr;
                points[i].startColor = new Color(1, 1, 1, percentage);
                points[i].startSize = percentage * starSize;
            }

            GetComponent<ParticleSystem>().SetParticles(points, points.Length);
        }


    }
}
