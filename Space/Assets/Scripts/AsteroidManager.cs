﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManager : MonoBehaviour {
    [SerializeField] Asteroid asteroid;
    [SerializeField] GameObject pickupPrefab;
    [SerializeField] int numberOfAsteroidsOnAnAxis = 10;
    [SerializeField] int gridSpacing = 100;

    void Start()
    {
        PlaceAsteroids();
    }

    void PlaceAsteroids()
    {
        for (int x= 0; x < numberOfAsteroidsOnAnAxis; x++)
        {

            for (int y = 0; y < numberOfAsteroidsOnAnAxis; y++)
            {

                for (int z = 0; z < numberOfAsteroidsOnAnAxis; z++)
                {
                    if (Random.Range(0, 10) < 0.01)
                    {
                        InstantiatePickupPrefab(x, y, z);
                    }
                    else
                    {
                        InstantiateAsteroid(x, y, z);
                    }
                }
            }
                
        }
    }

    void InstantiateAsteroid(int x, int y, int z)
    {
        Instantiate(asteroid, 
            new Vector3(
                transform.position.x + (x * gridSpacing) + AsteroidOffset(),
                transform.position.y + (y * gridSpacing) + AsteroidOffset(),
                transform.position.z + (z * gridSpacing) + AsteroidOffset())
            , Quaternion.identity, transform);
    }

    void InstantiatePickupPrefab(int x, int y, int z)
    {
        Instantiate(pickupPrefab,
           new Vector3(
               transform.position.x + (x * gridSpacing) + AsteroidOffset(),
               transform.position.y + (y * gridSpacing) + AsteroidOffset(),
               transform.position.z + (z * gridSpacing) + AsteroidOffset())
           , Quaternion.identity, transform);
    }


    float AsteroidOffset()
    {
        return Random.Range(-gridSpacing / 2f, gridSpacing / 2f);
    }

}
