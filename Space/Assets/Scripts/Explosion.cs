﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Explosion : MonoBehaviour {
    [SerializeField] GameObject explosion;
    [SerializeField] GameObject blowUp;
    [SerializeField] Rigidbody rigidBody;
    [SerializeField] PlayerShield shield;
    [SerializeField] Health health;
    [SerializeField] float laserHitModifier = 100f;

    /// <summary>
    /// Anounces that the current object has been hit.
    /// </summary>
    /// <param name="hitPosition">Hit position.</param>
	void IveBeenHit(Vector3 hitPosition)
    {
        GameObject go = Instantiate(explosion, hitPosition, Quaternion.identity, transform) as GameObject;
        Destroy(go, 3f);

        if (shield == null)
        {
            if (health != null)
            {
                health.TageDamage();
            }
            return;
        }
        else
        {
            shield.TageDamage();
        }
    }

    /// <summary>
    /// Adds force to rigidBody on hit.
    /// </summary>
    /// <param name="hitPosition">The position we've been hit at.</param>
    /// <param name="hitSource">The transform of the source that hit us.</param>
    public void AddForce(Vector3 hitPosition, Transform hitSource)
    {
        IveBeenHit(hitPosition);
        if (rigidBody == null)
            return;
        Vector3 forceVector = (hitSource.position - hitPosition).normalized;
        rigidBody.AddForceAtPosition(forceVector * laserHitModifier, hitPosition, ForceMode.Impulse);
        
    }

    /// <summary>
    /// Animates the blowup explosion of current object.
    /// </summary>
    public void BlowUp()
    {
        if (gameObject.tag.Equals("Player"))
        {
            EventManager.PlayerDeath(); // anounce the onPlayerDeath event
        } else if (gameObject.tag.Equals("Enemy"))
        {
            EventManager.EnemyDeath(transform.position);
        }
        // Instantiate explosion particle system.
        GameObject go  = Instantiate(blowUp, transform.position, Quaternion.identity) as GameObject;
        // Destroythe game object this script is attahed to.
        Destroy(gameObject, 1f);
        // Destroy player explosion
        Destroy(go, 3f);
        Destroy(gameObject);
       
    }

}
